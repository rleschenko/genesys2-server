'use strict';

module.exports = function(grunt) {
  // Load grunt tasks automatically
  require('load-grunt-tasks')(grunt);

  require('time-grunt')(grunt);
  require('jit-grunt')(grunt);

  grunt.initConfig({

    app : {
      source : 'src/main/sourceapp',
      dist : 'src/main/webapp/html'
    },
    clean : {
      dist : [ '.tmp', '<%= app.dist %>/styles', '<%= app.dist %>/js' ]
    },
    // Copies remaining files to places other tasks can use
    copy : {
      dist : {
        files : [ {
          expand : true,
          dot : true,
          cwd : '<%= app.source %>',
          dest : '<%= app.dist %>',
          src : [ '*.css', 'js/{,*/}*.js', '*.{ico,png,txt}', '.htaccess', '*.html', 'images/{,*/}*.{webp}', 'styles/fonts/{,*/}*.*' ]
        }, {
          expand : true,
          cwd : '.tmp/images',
          dest : '<%= app.dist %>/images',
          src : [ 'generated/*' ]
        }, {
          expand : true,
          cwd : 'bower_components/bootstrap-sass/assets/fonts/bootstrap/',
          src : '*',
          dest : '<%= app.dist %>/styles/fonts'
        }, {
          expand : true,
          cwd : 'bower_components/jquery-ui/themes/base/',
          src : [ 'jquery-ui.css' ],
          dest : '<%= app.dist %>/styles'
        }, {
          // tinyMCE
          expand : true,
          cwd : 'bower_components/tinymce/skins',
          src : [ '**' ],
          dest : '<%= app.dist %>/js/skins'
        }, {
          // leaflet
          expand : true,
          cwd : 'bower_components/leaflet/dist/',
          src : [ 'leaflet.css', 'images/*.png' ],
          dest : '<%= app.dist %>/styles'
        }, {
          // simplecolorpicker
          expand : true,
          cwd : 'bower_components/jquery-simplecolorpicker/',
          src : [ 'jquery.simplecolorpicker.css', 'jquery.simplecolorpicker-regularfont.css' ],
          dest : '<%= app.dist %>/styles'
        } ]
      },
      styles : {
        expand : true,
        cwd : '<%= app.source %>/styles',
        dest : '.tmp/styles/',
        src : '{,*/}*.css'
      }
    },
    // Compiles Sass to CSS and generates necessary files if requested
    compass : {
      options : {
        sassDir : '<%= app.source %>/styles',
        cssDir : '.tmp/styles',
        generatedImagesDir : '.tmp/images/generated',
        imagesDir : '<%= app.source %>/images',
        javascriptsDir : '<%= app.source %>/scripts',
        fontsDir : '<%= app.source %>/styles/fonts',
        importPath : './bower_components',
        httpImagesPath : '/images',
        httpGeneratedImagesPath : '/images/generated',
        httpFontsPath : '/styles/fonts',
        relativeAssets : false,
        assetCacheBuster : false,
        raw : 'Sass::Script::Number.precision = 10\n'
      },
      dist : {
        options : {
          cssDir : '<%= app.dist %>/styles',
          generatedImagesDir : '<%= app.dist %>/images/generated'
        }
      }
    // ,
    // server : {
    // options : {
    // sourcemap : true
    // }
    // }
    },

    // Make sure code styles are up to par and there are no obvious mistakes
    jshint : {
      options : {
        jshintrc : '.jshintrc',
        reporter : require('jshint-stylish')
      },
      all : {
        src : [ 'Gruntfile.js', '<%= app.source %>/js/{,*/}*.js' ]
      },
    // test: {
    // options: {
    // jshintrc: 'test/.jshintrc'
    // },
    // src: ['test/spec/{,*/}*.js']
    // }
    },
    concat : {
      options : {
      // separator: ';',
      },
      dist : {
        src : [ 'bower_components/jquery/dist/jquery.js', 'bower_components/modernizr/modernizr.js', 'bower_components/jquery-flot/jquery.flot.js', 'bower_components/jquery-flot/jquery.flot.pie.js',
          'bower_components/jquery-flot/jquery.flot.fillbetween.js',
          // simplecolorpicker
          'bower_components/jquery-simplecolorpicker/jquery.simplecolorpicker.js',
          // TinyMCE
          'bower_components/tinymce/tinymce.js', 'bower_components/tinymce/tinymce.jquery.js',
          // TinyMCE plugins: link, autolink, code
          'bower_components/tinymce/themes/modern/theme.js', 'bower_components/tinymce/plugins/link/plugin.js', 'bower_components/tinymce/plugins/autolink/plugin.js',
          'bower_components/tinymce/plugins/code/plugin.js',
          // Leaflet
          'bower_components/leaflet/dist/leaflet.js', 'bower_components/leaflet-locationfilter/src/locationfilter.js', 'bower_components/bootstrap-sass/assets/javascripts/bootstrap.js',
          'bower_components/jquery-ui/jquery-ui.js', 'bower_components/jquery-ui/ui/autocomplete.js',
          //
          'bower_components/dyn-css/lib/dyncss.js' ],
        dest : '<%= app.dist %>/js/libraries.js',
      },
      app : {
        src : [ '<%= app.source %>/js/main.js', '<%= app.source %>/js/crophub.js', '<%= app.source %>/js/custom.js' ],
        dest : '<%= app.dist %>/js/genesys.js'
      }
    },
    autoprefixer : {
      options : {
        browsers : [ 'last 3 versions' ]
      },
      dist : {
        files : [ {
          expand : true,
          cwd : '<%= app.dist %>/styles',
          src : '**/*.css',
          dest : '<%= app.dist %>/styles'
        } ]
      }
    },
    cssmin : {
      options : {
        keepSpecialComments : 0
      },
      dist : {
        files : {
          '<%= app.dist %>/styles/bootstrap.min.css' : [ '<%= app.dist %>/styles/bootstrap.css' ],
          '<%= app.dist %>/styles/other.min.css' : [ '<%= app.dist %>/styles/jquery-ui.css', '<%= app.dist %>/styles/forza.css', '<%= app.dist %>/styles/leaflet.css',
            '<%= app.dist %>/styles/jquery.simplecolorpicker.css', '<%= app.dist %>/styles/jquery.simplecolorpicker-regularfont.css' ],
          '<%= app.dist %>/styles/genesys.min.css' : [ '<%= app.dist %>/styles/genesys.css' ],
          '<%= app.dist %>/styles/all.min.css' : [ '<%= app.dist %>/styles/bootstrap.css', '<%= app.dist %>/styles/jquery-ui.css', '<%= app.dist %>/styles/forza.css',
            '<%= app.dist %>/styles/leaflet.css', '<%= app.dist %>/styles/genesys.css' ],
        }
      }
    },

    // JS min
    uglify : {
      dist : {
        options : {
          compress : false,
          preserveComments : false,
          report : 'min'
        },
        files : {
          '<%= app.dist %>/js/libraries.min.js' : [ '<%= app.dist %>/js/libraries.js' ],
          '<%= app.dist %>/js/genesys.min.js' : [ '<%= app.dist %>/js/genesys.js' ],
          '<%= app.dist %>/js/all.min.js' : [ '<%= app.dist %>/js/libraries.js', '<%= app.dist %>/js/genesys.js' ]
        }
      }
    }
  });

  // grunt.registerTask('serve', [ 'clean', 'copy:css', 'compass:server',
  // 'autoprefixer', 'uglify:server' ]);
  grunt.registerTask('build', [ 'clean', 'jshint:all', 'compass:dist', 'copy:dist', 'concat', 'autoprefixer', 'uglify:dist', 'cssmin' ]);
  grunt.registerTask('js', [ 'jshint:all', 'copy:dist', 'concat', 'autoprefixer', 'uglify:dist' ]);
  grunt.registerTask('css', [ 'compass:dist', 'copy:dist', 'concat', 'autoprefixer', 'cssmin' ]);
  grunt.registerTask('default', [ 'build' ]);

};
