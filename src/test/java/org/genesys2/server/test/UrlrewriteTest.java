/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.Test;

public class UrlrewriteTest {

	@Test
	public void testOnlyLang() {
		Pattern p = Pattern.compile("^/([^?]+)\\?lang=([a-z]{2})$");
		Matcher m;
		m = p.matcher("/hello.world");
		assertFalse(m.matches());
		m = p.matcher("/some/url?lang=de");
		assertTrue(m.matches());
		System.err.println(m.group(1));
		assertTrue("some/url".equals(m.group(1)));
		assertTrue("de".equals(m.group(2)));
	}
	
	@Test
	public void test1() {
		Pattern p = Pattern.compile("^/([^?]+)(?:\\?(.*)&|\\?)lang=([a-z]{2})(.*)$");
		Matcher m;
		m = p.matcher("/some/url?lang=de&foo=bar&1=2");
		assertTrue(m.matches());
		System.err.println(m.group(1));
		assertTrue("some/url".equals(m.group(1)));
		assertTrue(m.group(2) == null || "".equals(m.group(2)));
		assertTrue("de".equals(m.group(3)));
		assertTrue("&foo=bar&1=2".equals(m.group(4)));
		
		m = p.matcher("/some/url?demo=1,2,3&lang=de&foo=bar&1=2");
		assertTrue(m.matches());
		System.err.println(m.group(1));
		assertTrue("some/url".equals(m.group(1)));
		assertTrue("demo=1,2,3".equals(m.group(2)));
		assertTrue("de".equals(m.group(3)));
		assertTrue("&foo=bar&1=2".equals(m.group(4)));
		
		m = p.matcher("/some/url?demo=1,2,3&foo=bar&1=2&lang=de");
		assertTrue(m.matches());
		System.err.println(m.group(1));
		assertTrue("some/url".equals(m.group(1)));
		assertTrue("demo=1,2,3&foo=bar&1=2".equals(m.group(2)));
		assertTrue("de".equals(m.group(3)));
		assertTrue(m.group(4)==null || "".equals(m.group(4)));
		
	}
}
