package org.genesys2.server.model.filters;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.genesys2.server.service.impl.FilterHandler.AppliedFilters;
import org.genesys2.server.test.PropertyPlacholderInitializer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = InverseFiltersTest.Config.class, initializers = PropertyPlacholderInitializer.class)
@ActiveProfiles("dev")
public class InverseFiltersTest {
	public static final Log LOG = LogFactory.getLog(InverseFiltersTest.class);

	public static class Config {
		
		@Bean
		public ObjectMapper objectMapper() {
			return new ObjectMapper();
		}
	}

	@Autowired
	private ObjectMapper objectMapper;

	@Test
	public void testDeserializeNeg() throws JsonParseException, JsonMappingException, IOException {
		String source = "{\"key\":[1,{\"min\":2},\"Test\"],\"-key2\":[{\"max\":2.1},{\"range\":[-1,3.0]}],\"key3\":[{\"like\":\"Te\"},null]}";
		AppliedFilters afs = objectMapper.readValue(source, AppliedFilters.class);

		String result = objectMapper.writeValueAsString(afs);
		LOG.info(result);
		assertTrue(source.equals(result));
		
		assertTrue("-key2 must be a negative filter", afs.get("key2").isInverse());
		assertFalse("key must not be a negative filter", afs.get("key").isInverse());
	}
}
