/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.model.impl;

import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.junit.Test;

public class FaoInstituteTest {

	@Test
	public void testUrlsDefault() throws MalformedURLException {
		FaoInstitute i = new FaoInstitute();
		assertTrue(i.getUrl() == null);
		assertTrue(i.getUrls() == null);
	}

	@Test
	public void testUrlsBlank() throws MalformedURLException {
		FaoInstitute i = new FaoInstitute();
		i.setUrl("");
		assertTrue(i.getUrl() != null);
		assertTrue(i.getUrls() == null);
	}

	@Test
	public void testUrlsBlank2() throws MalformedURLException {
		FaoInstitute i = new FaoInstitute();
		i.setUrl(";");
		assertTrue(i.getUrl() != null);
		assertTrue(i.getUrls() != null);
		assertTrue(i.getUrls().size() == 0);
	}

	@Test
	public void testUrlsValid1() throws MalformedURLException {
		FaoInstitute i = new FaoInstitute();
		i.setUrl("https://www.iita.org");
		assertTrue(i.getUrl() != null);
		assertTrue(i.getUrls() != null);
		assertTrue(i.getUrls().size() == 1);
	}

	@Test
	public void testUrlsValid2() throws MalformedURLException {
		FaoInstitute i = new FaoInstitute();
		i.setUrl("https://www.iita.org; http://genebank.iita.org, ");
		assertTrue(i.getUrl() != null);
		List<URL> urls = i.getUrls();
		assertTrue(urls != null);
		assertTrue(urls.size() == 2);
	}

	@Test
	public void testUrlsNoProtocol() throws MalformedURLException {
		FaoInstitute i = new FaoInstitute();
		i.setUrl("www.iita.org");
		assertTrue(i.getUrl() != null);
		List<URL> urls = i.getUrls();
		assertTrue(urls != null);
		assertTrue(urls.size() == 1);
		assertTrue(urls.get(0).equals(new URL("http://www.iita.org")));
	}

	@Test
	public void testUrlsNoProtocol2() throws MalformedURLException {
		FaoInstitute i = new FaoInstitute();
		i.setUrl("www.iita.org,https://foo.bar;www.somehost.org");
		assertTrue(i.getUrl() != null);
		List<URL> urls = i.getUrls();
		assertTrue(urls != null);
		assertTrue(urls.size() == 3);
		assertTrue(urls.get(0).equals(new URL("http://www.iita.org")));
		assertTrue(urls.get(1).equals(new URL("https://foo.bar")));
		assertTrue(urls.get(2).equals(new URL("http://www.somehost.org")));
	}

	@Test
	// http://www.sasa.gov.uk www.scottishlandraces.org.uk
	// www.varieties.potato.org.uk www.agricrops.org
	public void testUrlsSample1() throws MalformedURLException {
		FaoInstitute i = new FaoInstitute();
		i.setUrl("http://www.sasa.gov.uk  www.scottishlandraces.org.uk  www.varieties.potato.org.uk  www.agricrops.org");
		assertTrue(i.getUrl() != null);
		List<URL> urls = i.getUrls();
		assertTrue(urls != null);
		assertTrue(urls.size() == 4);
		assertTrue(urls.get(0).equals(new URL("http://www.sasa.gov.uk")));
		assertTrue(urls.get(1).equals(new URL("http://www.scottishlandraces.org.uk")));
		assertTrue(urls.get(2).equals(new URL("http://www.varieties.potato.org.uk")));
		assertTrue(urls.get(3).equals(new URL("http://www.agricrops.org")));
	}

	@Test
	public void testDump1() throws IOException {
		InputStream fis = getClass().getResourceAsStream("/org/genesys2/server/model/impl/wiews-urls.txt");
		BufferedReader sr = new BufferedReader(new InputStreamReader(fis));

		String l = null;
		FaoInstitute i = new FaoInstitute();

		while ((l = sr.readLine()) != null) {
			int expectedUrls = StringUtils.isBlank(l) ? 0 : l.trim().split("[,;\\s]+").length;
			i.setUrl(l);
			try {
				
				List<URL> urls = i.getUrls();
				if (StringUtils.isBlank(l)) {
					assertTrue(urls == null);
				} else {
					assertTrue(urls != null);
					assertTrue(urls.size() == expectedUrls);
				}
			} catch (MalformedURLException e) {
				System.err.println(e.getMessage());
				System.err.println(l);
				System.err.println(Arrays.asList(l.trim().split("[,;\\s]+")));
			}
		}
		IOUtils.closeQuietly(fis);
	}
	
	@Test
	public void testFunny1() throws MalformedURLException {
		FaoInstitute i=new FaoInstitute();
		i.setUrl("www.http://ongrc.org");
		List<URL> urls = i.getUrls();
		assertTrue(urls.get(0).equals(new URL("http://ongrc.org")));
		
		i.setUrl("htt://www.conagebio.go.cr");
		urls = i.getUrls();
		assertTrue(urls.get(0).equals(new URL("http://www.conagebio.go.cr")));
	}
}
