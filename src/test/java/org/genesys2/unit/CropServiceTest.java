/**
 * Copyright 2015 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/
package org.genesys2.unit;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.genesys2.server.model.genesys.Taxonomy2;
import org.genesys2.server.model.impl.Crop;
import org.genesys2.server.model.impl.CropRule;
import org.genesys2.server.model.impl.CropTaxonomy;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class CropServiceTest extends AbstractServicesTest {
	private static final Log LOG = LogFactory.getLog(CropServiceTest.class);

	@Before
	public void setUp() {
		cropService.addCrop("Crop short name", "Crop name", "description", "en");
	}

	@After
	public void teardown() {
		cropTaxonomyRepository.deleteAll();
		cropRepository.deleteAll();
		cropRuleRepository.deleteAll();
	}

	@Test
	public void getCropTest() {
		LOG.info("Start test-method getCropTest");

		assertNotNull(cropService.getCrop("Crop short name"));

		LOG.info("Test getCropTest passed!");
	}

	@Test
	public void deleteTest() {
		LOG.info("Start test-method deleteTest");

		Crop cropForRemove = cropService.getCrop("Crop short name");
		cropService.delete(cropForRemove);
		assertTrue(cropService.listCrops().isEmpty());

		LOG.info("Test deleteTest passed!");
	}

	@Test
	public void updateCropTest() {
		LOG.info("Start test-method updateCropTest");

		Crop cropForUpdate = cropService.getCrop("Crop short name");
		cropService.updateCrop(cropForUpdate, "new name", "new description", "ru");
		assertTrue(cropService.getCrop("Crop short name").getName().equals("new name"));

		LOG.info("Test updateCropTest passed!");
	}

	@Test
	public void addCropRuleTest() {
		LOG.info("Start test-method addCropRuleTest");

		Crop cropForTest = cropService.getCrop("Crop short name");

		cropService.addCropRule(cropForTest, "genus", "species", false);
		assertTrue(!cropService.getCropRules(cropForTest).isEmpty());

		LOG.info("Test addCropRuleTest passed!");
	}

	@Test
	public void setCropRulesTest() {
		LOG.info("Start test-method setCropRulesTest");

		Crop cropForTest = cropService.getCrop("Crop short name");

		List<CropRule> cropRuleList = new ArrayList<>();
		CropRule cropRule = new CropRule();
		cropRule.setGenus("genus");
		cropRuleList.add(cropRule);

		cropService.setCropRules(cropForTest, cropRuleList);
		assertTrue(!cropService.getCropRules(cropForTest).isEmpty());

		LOG.info("Test setCropRulesTest passed!");
	}

	@Test
	public void getCropRulesTest() {
		LOG.info("Start test-method getCropRulesTest");

		Crop cropForTest = cropService.getCrop("Crop short name");

		assertTrue(cropService.getCropRules(cropForTest).isEmpty());

		List<CropRule> cropRuleList = new ArrayList<>();
		CropRule cropRule = new CropRule();
		cropRule.setGenus("genus");
		cropRuleList.add(cropRule);

		cropService.setCropRules(cropForTest, cropRuleList);
		assertTrue(!cropService.getCropRules(cropForTest).isEmpty());

		LOG.info("Test getCropRulesTest passed!");
	}

	@Test
	public void updateCropTaxonomyListsTest() {
		LOG.info("Start test-method updateCropTaxonomyListsTest");

		Taxonomy2 taxonomy = new Taxonomy2();
		taxonomy.setGenus("genus");
		taxonomy.setVersion(1);
		taxonomy.setSpAuthor("SpAuthor");
		taxonomy.setSpecies("species");
		taxonomy.setSubtAuthor("SubtAuthor");
		taxonomy.setSubtaxa("Subtaxa");
		taxonomy.setTaxonName("TaxonName");

		Crop cropForTest = cropService.getCrop("Crop short name");

		Crop crop2 = cropService.addCrop("Crop short name 2", "Crop name 2", "description 2", "en");

		List<CropRule> cropRuleList = new ArrayList<>();
		CropRule cropRule = new CropRule();
		cropRule.setGenus("genus");
		cropRule.setSpecies("species");
		cropRule.setSubtaxa("Subtaxa");
		cropRule.setCrop(cropForTest);
		cropRule.setIncluded(true);

		CropRule cropRule2 = new CropRule();
		cropRule2.setGenus("genuss");
		cropRule2.setSpecies("speciess");
		cropRule2.setSubtaxa("Subtaxas");
		cropRule2.setCrop(cropForTest);
		cropRule2.setIncluded(true);

		cropRuleList.add(cropRule2);
		cropRuleList.add(cropRule);

		cropService.setCropRules(cropForTest, cropRuleList);

		List<CropTaxonomy> cropTaxonomyList = new ArrayList<>();

		CropTaxonomy cropTaxonomy = new CropTaxonomy();
		cropTaxonomy.setTaxonomy(taxonomy);
		cropTaxonomy.setCrop(cropForTest);

		CropTaxonomy cropTaxonomy2 = new CropTaxonomy();
		cropTaxonomy2.setTaxonomy(taxonomy);
		cropTaxonomy2.setCrop(crop2);

		cropTaxonomyList.add(cropTaxonomy);
		cropTaxonomyList.add(cropTaxonomy2);

		taxonomy.setCropTaxonomies(cropTaxonomyList);

		taxonomy2Repository.save(taxonomy);

		cropTaxonomyRepository.save(cropTaxonomyList);

		assertTrue(cropTaxonomyRepository.findAll().size() == 2);

		cropService.updateCropTaxonomyLists(taxonomy);

		assertTrue(cropTaxonomyRepository.findAll().size() == 1);

		LOG.info("Test updateCropTaxonomyListsTest passed!");
	}
}
