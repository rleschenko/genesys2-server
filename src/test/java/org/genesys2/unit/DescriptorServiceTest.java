/**
 * Copyright 2015 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/
package org.genesys2.unit;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.genesys2.server.model.impl.Descriptor;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class DescriptorServiceTest extends AbstractServicesTest {

	private static final Log LOG = LogFactory.getLog(DescriptorServiceTest.class);

	private Descriptor descriptor;

	@Before
	public void setUp() {
		descriptor = new Descriptor();
		descriptor.setCode("code of descriptor");

		descriptorService.saveDescriptor(descriptor);
	}

	@After
	public void teardown() {
		descriptorRepository.deleteAll();
	}

	@Test
	public void listTest() {
		LOG.info("Start test-method listTest");

		assertTrue(descriptorService.list().size() == 1);

		LOG.info("Test listTest passed!");
	}

	@Test
	public void saveDescriptorTest() {
		LOG.info("Start test-method saveDescriptorTest");

		assertTrue(descriptorService.list().size() == 1);

		Descriptor newDescriptor = new Descriptor();
		newDescriptor.setCode("code of newDescriptor");

		descriptorService.saveDescriptor(newDescriptor);

		assertTrue(descriptorService.list().size() == 2);

		LOG.info("Test saveDescriptorTest passed!");
	}

	@Test
	public void getDescriptor() {
		LOG.info("Start test-method getDescriptor");

		Descriptor descriptorForTest = descriptorService.getDescriptor(descriptor.getCode());

		assertTrue(descriptorForTest != null);
		assertTrue(descriptorForTest.getCode().equals("code of descriptor"));

		LOG.info("Test getDescriptor passed!");
	}
}
