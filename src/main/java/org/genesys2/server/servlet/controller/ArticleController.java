/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.servlet.controller;

import java.io.IOException;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;

import org.genesys2.server.model.impl.Article;
import org.genesys2.server.service.ContentService;
import org.genesys2.spring.RequestAttributeLocaleResolver;
import org.genesys2.spring.ResourceNotFoundException;
import org.genesys2.transifex.client.TransifexException;
import org.genesys2.transifex.client.TransifexService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@Controller
@RequestMapping("/content")
public class ArticleController extends BaseController {

	@Autowired
	private ContentService contentService;

	@Autowired(required = false)
	private TransifexService transifexService;

	@Autowired
	private RequestAttributeLocaleResolver localeResolver;

	private ObjectMapper mapper = new ObjectMapper();

	@RequestMapping(value = "/transifex", params = { "post" }, method = RequestMethod.POST)
	@PreAuthorize("hasRole('ADMINISTRATOR') or hasRole('CONTENTMANAGER')")
	public String postTotransifex(@RequestParam("slug") String slug, Model model) {
		if (transifexService == null) {
			throw new ResourceNotFoundException("translationService not enabled");
		}
		Article article = contentService.getGlobalArticle(slug, getLocale());
		String resourceName = "article-" + slug;
		try {
			if (transifexService.resourceExists(resourceName)) {
				transifexService.updateXhtmlResource(resourceName, article.getTitle(), article.getBody());
			} else {
				transifexService.createXhtmlResource(resourceName, article.getTitle(), article.getBody());
			}
		} catch (IOException e) {
			model.addAttribute("responseFromTransifex", "article.transifex-failed");
			model.addAttribute("article", article);
			e.printStackTrace();
		}
		model.addAttribute("responseFromTransifex", "article.transifex-resource-updated");
		model.addAttribute("article", article);
		return "/content/article-edit";
	}

	@RequestMapping(value = "/transifex", params = { "remove" }, method = RequestMethod.POST)
	@PreAuthorize("hasRole('ADMINISTRATOR') or hasRole('CONTENTMANAGER')")
	public String deleteFromtransifex(@RequestParam("slug") String slug, Model model) {
		if (transifexService == null) {
			throw new ResourceNotFoundException("translationService not enabled");
		}

		Article article = contentService.getGlobalArticle(slug, getLocale());
		model.addAttribute("article", article);

		try {
			if (transifexService.deleteResource("article-" + slug)) {
				model.addAttribute("responseFromTransifex", "article.transifex-resource-removed");
			} else {
				model.addAttribute("responseFromTransifex", "article.transifex-failed");
			}
		} catch (TransifexException e) {
			_logger.error(e.getMessage(), e);
			model.addAttribute("responseFromTransifex", "article.transifex-failed");
		}

		return "/content/article-edit";
	}

	@RequestMapping(value = "/translate/{slug}/{language}", method = RequestMethod.GET)
	@PreAuthorize("hasRole('ADMINISTRATOR') or hasRole('CONTENTMANAGER')")
	public String fetchFromtransifex(@PathVariable("slug") String slug, @PathVariable("language") String language, Model model) throws Exception {
		if (transifexService == null) {
			throw new ResourceNotFoundException("translationService not enabled");
		}

		Locale locale = new Locale(language);

		Article article = contentService.getGlobalArticle(slug, locale, false);

		String translatedResource;
		try {
			translatedResource = transifexService.getTranslatedResource("article-" + slug, locale);
		} catch (TransifexException e) {
			_logger.error(e.getMessage(), e);
			throw new Exception(e.getMessage(), e);
		}

		String title = null;
		String body = null;

		try {
			JsonNode jsonObject = mapper.readTree(translatedResource);
			String content = jsonObject.get("content").asText();
			title = content.split("<title>")[1].split("</title>")[0];
			body = content.split("<body>")[1].split("</body>")[0];
		} catch (IOException e) {
			_logger.error(e.getMessage(), e);
			throw e;
		}

		if (article == null) {
			article = new Article();
			article.setSlug(slug);
		}

		article.setTitle(title);
		article.setBody(body);
		article.setLang(language);

		model.addAttribute("article", article);
		return "/content/article-edit";
	}

	@RequestMapping
	@PreAuthorize("hasRole('ADMINISTRATOR') or hasRole('CONTENTMANAGER')")
	public String list(ModelMap model, @RequestParam(value = "page", defaultValue = "1") int page,
			@RequestParam(value = "language", defaultValue = "") String lang) {
		if (!lang.isEmpty()) {
			model.addAttribute("pagedData", contentService.listArticlesByLang(lang, new PageRequest(page - 1, 50, new Sort("slug"))));
		} else {
			model.addAttribute("pagedData", contentService.listArticles(new PageRequest(page - 1, 50, new Sort("slug"))));
		}

		// todo full name of locales
		Map<String, String> locales = new TreeMap<>();

		for (String language : localeResolver.getSupportedLocales()) {
			locales.put(new Locale(language).getDisplayName(), language);
		}

		model.addAttribute("languages", locales);

		return "/content/index";
	}

	@RequestMapping("{url:.+}")
	public String view(ModelMap model, @PathVariable(value = "url") String slug) {
		_logger.debug("Viewing article " + slug);

		final Article article = contentService.getGlobalArticle(slug, getLocale());
		if (article == null) {
			if (hasRole("ADMINISTRATOR")) {
				return "redirect:/content/" + slug + "/edit";
			}
			throw new ResourceNotFoundException();
		}
		model.addAttribute("title", article.getTitle());
		model.addAttribute("article", article);

		return "/content/article";
	}

	/**
	 * Edit article in current language
	 * 
	 * @param model
	 * @param slug
	 * @return
	 */
	@PreAuthorize("hasRole('ADMINISTRATOR') or hasRole('CONTENTMANAGER')")
	@RequestMapping("{url:.+}/edit")
	public String edit(ModelMap model, @PathVariable(value = "url") String slug) {
		_logger.debug("Editing article " + slug);
		return "redirect:/content/" + slug + "/edit/" + LocaleContextHolder.getLocale().getLanguage();
	}

	/**
	 * Edit article in another language
	 * 
	 * @param model
	 * @param slug
	 * @param language
	 * @return
	 */
	@PreAuthorize("hasRole('ADMINISTRATOR') or hasRole('CONTENTMANAGER')")
	@RequestMapping("{url:.+}/edit/{language}")
	public String edit(ModelMap model, @PathVariable(value = "url") String slug, @PathVariable("language") String language) {
		_logger.debug("Editing article " + slug);

		Article article = contentService.getArticleBySlugAndLang(slug, language);
		if (article == null) {
			article = new Article();
			article.setSlug(slug);
			article.setLang(language);
		}
		model.addAttribute("article", article);

		return "/content/article-edit";
	}

	@PreAuthorize("hasRole('ADMINISTRATOR') or hasRole('CONTENTMANAGER')")
	@RequestMapping(value = "/save-article/{language}", method = { RequestMethod.POST })
	public String createNewGlobalArticle(ModelMap model, @RequestParam("slug") String slug, @PathVariable("language") String language,
			@RequestParam("title") String title, @RequestParam("body") String body) {

		contentService.createGlobalArticle(slug, new Locale(language), title, body);

		return redirectAfterSave(slug, language);
	}

	private String redirectAfterSave(String slug, String language) {
		if (LocaleContextHolder.getLocale().getLanguage().equals(language)) {
			return "redirect:/content/" + slug;
		} else {
			return "redirect:/content/?language=" + language;
		}
	}

	@PreAuthorize("hasRole('ADMINISTRATOR') or hasRole('CONTENTMANAGER')")
	@RequestMapping(value = "/save-article/{language}", params = { "id" }, method = { RequestMethod.POST })
	public String saveExistingGlobalArticle(ModelMap model, @PathVariable("language") String language, @RequestParam("id") long id,
			@RequestParam("slug") String slug, @RequestParam("title") String title, @RequestParam("body") String body) {

		Article article = contentService.updateArticle(id, slug, title, body);

		return redirectAfterSave(slug, article.getLang());
	}

	@RequestMapping(value = "/blurp/update-blurp", method = { RequestMethod.POST })
	public String updateBlurp(ModelMap model, @RequestParam("id") long id, @RequestParam(required = false, value = "title") String title,
			@RequestParam("body") String body) {

		contentService.updateArticle(id, null, title, body);

		return "redirect:/";
	}

	@RequestMapping(value = "/blurp/create-blurp", method = { RequestMethod.POST })
	public String createBlurp(ModelMap model, @RequestParam("clazz") String clazz, @RequestParam("entityId") long entityId,
			@RequestParam(required = false, value = "title") String title, @RequestParam("body") String body) throws ClassNotFoundException {

		contentService.updateArticle(Class.forName(clazz), entityId, "blurp", title, body, getLocale());
		return "redirect:/";
	}
}
