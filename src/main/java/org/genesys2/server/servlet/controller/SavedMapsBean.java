/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.servlet.controller;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.genesys2.server.servlet.model.SavedMap;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("session")
public class SavedMapsBean implements Serializable {

    private static final long serialVersionUID = -1084615112110837714L;

    private final List<SavedMap> savedMaps = new ArrayList<>();

    public List<SavedMap> getSavedMaps() {
        return savedMaps;
    }

    public SavedMap newSavedMap(String title) {
        SavedMap savedMap = new SavedMap();
        savedMap.setTitle(title);
        return savedMaps.get(savedMaps.indexOf(savedMap));
    }

    public void addSavedMap(SavedMap filter) {
        savedMaps.add(filter);
    }
}
