package org.genesys2.server.servlet.controller.rest;

import org.genesys2.server.service.impl.RESTApiException;

public class PleaseRetryException extends RESTApiException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 456286423902987649L;

	public PleaseRetryException(String message, Throwable cause) {
		super(message, cause);
	}

}
