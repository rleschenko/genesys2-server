/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.servlet.controller.rest;

import org.genesys2.server.model.acl.AclObjectIdentity;
import org.genesys2.server.model.acl.AclSid;
import org.genesys2.server.model.impl.User;
import org.genesys2.server.model.oauth.OAuthAccessToken;
import org.genesys2.server.model.oauth.OAuthClientDetails;
import org.genesys2.server.model.oauth.OAuthRefreshToken;
import org.genesys2.server.service.AclService;
import org.genesys2.server.service.JPATokenStore;
import org.genesys2.server.service.OAuth2ClientDetailsService;
import org.genesys2.server.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@Controller("restOAuthManagementController")
@PreAuthorize("isAuthenticated()")
@RequestMapping(value = {"/api/v0/oauth", "/json/v0/oauth"})
public class OAuthManagementController extends RequestsController {

    final static String CLAZZ = OAuthClientDetails.class.getName();

    @Value("${base.url}")
    private String baseUrl;

    @Autowired
    protected UserService userService;

    @Autowired
    private OAuth2ClientDetailsService clientDetailsService;

    @Autowired
    private AclService aclService;

    @Autowired
    @Qualifier("tokenStore")
    private JPATokenStore tokenStore;

    @PreAuthorize("hasRole('ADMINISTRATOR')")
    @RequestMapping("/clientslist")
    @ResponseBody
    public Object listClients() {
        return clientDetailsService.listClientDetails();
    }


    @RequestMapping("/token/at/{tokenId}/remove")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    @ResponseBody
    public String removeAccessToken(@PathVariable("tokenId") long tokenId) {
        tokenStore.removeAccessToken(tokenId);
        return JSON_OK;
    }

    @PreAuthorize("hasRole('ADMINISTRATOR')")
    @RequestMapping("/token/rt/{tokenId}/remove")
    @ResponseBody
    public String removeRefreshToken(@PathVariable("tokenId") long tokenId) {

        tokenStore.removeRefreshToken(tokenId);
        return JSON_OK;
    }

    @PreAuthorize("hasRole('ADMINISTRATOR')")
    @RequestMapping("/token/{clientId:.+}/remove-all-at")
    @ResponseBody
    public String removeAllAccessTokens(@PathVariable("clientId") String clientId) {

        final Collection<OAuth2AccessToken> tokens = tokenStore.findTokensByClientId(clientId);
        for (final OAuth2AccessToken token : tokens) {
            tokenStore.removeAccessToken(token);
        }

        return JSON_OK;
    }

    @PreAuthorize("hasRole('ADMINISTRATOR')")
    @RequestMapping("/token/{clientId:.+}/remove-all-rt")
    @ResponseBody
    public String removeAllRefreshTokens(@PathVariable("clientId") String clientId) {

        final Collection<OAuthRefreshToken> tokens = tokenStore.findRefreshTokensByClientId(clientId);
        for (final OAuthRefreshToken token : tokens) {
            tokenStore.removeRefreshToken(token.getId());
        }

        return JSON_OK;
    }


    @PreAuthorize("hasAnyRole('VETTEDUSER','ADMINISTRATOR')")
    @RequestMapping(value = "/save-client", method = RequestMethod.POST)
    @ResponseBody
    public String saveClientEntry(@RequestBody OAuthClientDetails requestClient) {

        OAuthClientDetails clientDetails;

        if (requestClient.getId() == null) {
            clientDetails = clientDetailsService.addClientDetails(
                    requestClient.getTitle(),
                    requestClient.getDescription(),
                    requestClient.getRedirectUris(),
                    requestClient.getAccessTokenValiditySeconds(),
                    requestClient.getRefreshTokenValiditySeconds(),
                    null);
        } else {
            clientDetails = clientDetailsService.update(clientDetailsService.getClientDetails(requestClient.getId()),
                    requestClient.getTitle(),
                    requestClient.getDescription(),
                    requestClient.getClientSecret(),
                    requestClient.getRedirectUris(),
                    requestClient.getAccessTokenValiditySeconds(),
                    requestClient.getRefreshTokenValiditySeconds());
        }

        return "{\"clientId\":\"" + clientDetails.getClientId() + "\"}";
    }


    @PreAuthorize("hasAnyRole('VETTEDUSER','ADMINISTRATOR')")
    @RequestMapping(value = "/delete-client", method = RequestMethod.POST)
    @ResponseBody
    public String deleteClient(@RequestBody OAuthClientDetails requestClient) {
        final OAuthClientDetails clientDetails = clientDetailsService.getClientDetails(requestClient.getId());
        LOG.info("Deleting client " + clientDetails.getClientId());
        clientDetailsService.removeClient(clientDetails);
        return JSON_OK;
    }


//    @PreAuthorize("hasRole('ADMINISTRATOR')")
//    @RequestMapping(value = "/{clientId:.+}", method = RequestMethod.GET)
//    @ResponseBody
//    public Object clientDetails(@PathVariable("clientId") String clientId) {
//        ClientDetails clientDetails = clientDetailsService.loadClientByClientId(clientId);
//        return clientDetails;
//    }

    // id will need for permissions
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public Object clientDetails(@PathVariable("id") long id) {
        ClientDetails clientDetails = clientDetailsService.getClientDetails(id);
        return clientDetails;
    }


    @PreAuthorize("hasRole('ADMINISTRATOR')")
    @RequestMapping(value = "/tokens/{clientId:.+}", method = RequestMethod.GET)
    @ResponseBody
    public Object clientTokens(@PathVariable("clientId") String clientId) {
        Collection<OAuthAccessToken> tokensByClientId = clientDetailsService.findTokensByClientId(clientId);
        Collection<OAuthRefreshToken> refreshTokensByClientId = clientDetailsService.findRefreshTokensClientId(clientId);

        HashMap<String, Object> tokensMap = new HashMap<>();
        tokensMap.put("accessTokens", tokensByClientId);
        tokensMap.put("refreshTokens", refreshTokensByClientId);

        return tokensMap;
    }


    @PreAuthorize("hasRole('ADMINISTRATOR')")
    @RequestMapping(value = "/permissions/{id}", method = RequestMethod.GET)
    @ResponseBody
    public Object getPermissions(@PathVariable("id") long id) {

        Map<String, Object> resultMap = new HashMap<>();

        final AclObjectIdentity objectIdentity = aclService.ensureObjectIdentity(CLAZZ, id);
        resultMap.put("aclObjectIdentity", objectIdentity);

        if (objectIdentity != null) {
            resultMap.put("aclPermissions", aclService.getAvailablePermissions(CLAZZ));
        }

        List<AclSid> aclSids = aclService.getSids(id, CLAZZ);

        resultMap.put("aclSids", aclSids);
        resultMap.put("aclEntries", aclService.getPermissions(id, CLAZZ));

        Set<User> users = new HashSet<>();
        users.add(userService.getUserByUuid(objectIdentity.getOwnerSid().getSid()));

        for (AclSid sid: aclSids){
            users.add(userService.getUserByUuid(sid.getSid()));
        }

        resultMap.put("users", users);

        return resultMap;

    }

    @RequestMapping(value = "/client/{clientId:.+}/get_widget")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    @ResponseBody
    public Object getWidget(@PathVariable(value = "clientId") String clientId) {

        Map<String, Object> resultMap = new HashMap<>();

        if (!clientId.equals("")) {
            ClientDetails clientDetails = clientDetailsService.loadClientByClientId(clientId);
            String script =
                    "<script>(function(d, s, id) {\n" +
                            "var js, gjs = d.getElementsByTagName(s)[0];\n" +
                            "if (d.getElementById(id)) return;\n" +
                            "js = d.createElement(s); js.id = id;\n" +
                            "js.src = '" + baseUrl + "/webapi/genesys-webapi.js?client_id=" + clientDetails.getClientId() + "&client_secret=" + clientDetails.getClientSecret() + "';\n" +
                            "gjs.parentNode.insertBefore(js, gjs);\n" +
                            "}(document, 'script', 'genesys-api'));</script>";

            resultMap.put("client", clientDetails);
            resultMap.put("script", script);
        }

        List<OAuthClientDetails> clientDetailses = clientDetailsService.listClientDetails();
        resultMap.put("clientDetails", clientDetailses);

        return resultMap;
    }

}
