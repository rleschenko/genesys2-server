package org.genesys2.server.servlet.controller.admin;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/admin/logger")
@PreAuthorize("hasRole('ADMINISTRATOR')")
public class LoggerController {

	public static final Log LOG = LogFactory.getLog(AdminController.class);

	@Value("${paginator.default.pageSize}")
	int defaultPageSize;

	@RequestMapping(method = RequestMethod.GET, value = "/")
	public String adjustLogger(Model model, @RequestParam(value = "page", defaultValue = "1") int page) {
		// first element on page
		int firstelement = (page - 1) * defaultPageSize;

		List<Logger> allLoggers = getAllLoggers();
		List<Logger> loggers = new ArrayList<>();

		// copy part of array
		for (int i = 0; i < defaultPageSize && i < allLoggers.size(); i++) {
			loggers.add(allLoggers.get(firstelement + i));
		}

		PageImpl<Logger> pagedData = new PageImpl<Logger>(loggers, new PageRequest(page, defaultPageSize), allLoggers.size());
		model.addAttribute("loggers", pagedData);
		return "/admin/logger/index";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{loggerName}")
	public String adjustLoggerPage(Model model, @PathVariable(value = "loggerName") String loggerName) {
		Logger logger;

		if (loggerName != null && "root".equalsIgnoreCase(loggerName))
			logger = LogManager.getRootLogger();
		else
			logger = LogManager.getLogger(loggerName);

		model.addAttribute("logger", logger);
		model.addAttribute("appenders", logger.getRootLogger().getAllAppenders());
		return "/admin/logger/edit";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/changeLoger")
	public String changeLogger(@RequestParam(value = "loggerLevel") String loggerLevel, @RequestParam(value = "loggerName") String loggerName) {
		Logger logger;

		if (loggerName == null || "root".equalsIgnoreCase(loggerName)) {
			logger = LogManager.getRootLogger();
		} else {
			logger = LogManager.getLogger(loggerName);
		}

		if (logger != null) {
			LOG.debug("Got logger: " + logger.getName());
			logger.setLevel(loggerLevel == null ? null : Level.toLevel(loggerLevel));
		}

		return "redirect:/admin/logger/" + loggerName + ".";
	}

	private List<Logger> getAllLoggers() {

		Enumeration<Logger> en = LogManager.getCurrentLoggers();
		List<Logger> loggers = new ArrayList<>();

		while (en.hasMoreElements()) {
			Logger logger = en.nextElement();
			if (logger.getLevel() != null)
				loggers.add(logger);
		}

		loggers.add(LogManager.getRootLogger());

		Collections.sort(loggers, new Comparator<Logger>() {
			@Override
			public int compare(Logger o1, Logger o2) {
				// root logger is on the top
				if (LogManager.getRootLogger() == o1) {
					return -1;
				}
				if (LogManager.getRootLogger() == o2) {
					return 1;
				}
				// otherwise sort by name
				return o1.getName().compareTo(o2.getName());
			}
		});

		return loggers;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/addLoger")
	public String addLogger(@RequestParam(value = "nameNewLogger") String nameNewLogger, Model model) {
		Logger logger = Logger.getLogger(nameNewLogger);
		logger.setLevel(Level.INFO);

		return "redirect:/admin/logger/";
	}

}
