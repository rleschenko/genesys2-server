/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.servlet.util;

import static org.springframework.security.acls.domain.BasePermission.ADMINISTRATION;
import static org.springframework.security.acls.domain.BasePermission.CREATE;
import static org.springframework.security.acls.domain.BasePermission.DELETE;
import static org.springframework.security.acls.domain.BasePermission.READ;
import static org.springframework.security.acls.domain.BasePermission.WRITE;

import java.util.HashMap;
import java.util.Map;

import org.genesys2.server.servlet.model.PermissionJson;

public final class PermissionJsonUtil {
	private PermissionJsonUtil() {
	}

	public static Map<Integer, Boolean> createPermissionsMap(PermissionJson permissionJson) {
		final Map<Integer, Boolean> permissionMap = new HashMap<>();
		permissionMap.put(CREATE.getMask(), permissionJson.isCreate());
		permissionMap.put(READ.getMask(), permissionJson.isRead());
		permissionMap.put(WRITE.getMask(), permissionJson.isWrite());
		permissionMap.put(DELETE.getMask(), permissionJson.isDelete());
		permissionMap.put(ADMINISTRATION.getMask(), permissionJson.isManage());
		return permissionMap;
	}

}
