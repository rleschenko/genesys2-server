/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.listener;

import java.util.concurrent.Callable;

import org.genesys2.server.security.AsAdminInvoker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class RunAsAdminListener implements InitializingBean {

	@Autowired
	protected AsAdminInvoker asAdminInvoker;

	protected Logger _logger = LoggerFactory.getLogger(getClass());

	// in initializing stage @AsAdmin doesn't work properly
	@Override
	public void afterPropertiesSet() throws Exception {
		asAdminInvoker.invoke(new Callable<Object>() {
			@Override
			public Void call() throws Exception {
				init();
				return null;
			}
		});
	}

	protected abstract void init() throws Exception;
}
