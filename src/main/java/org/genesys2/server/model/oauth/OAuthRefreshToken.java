/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.model.oauth;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.genesys2.server.model.BusinessModel;
import org.springframework.security.oauth2.common.DefaultExpiringOAuth2RefreshToken;
import org.springframework.security.oauth2.common.DefaultOAuth2RefreshToken;
import org.springframework.security.oauth2.common.OAuth2RefreshToken;

@Entity
@Table(name = "oauthrefreshtoken")
public class OAuthRefreshToken extends BusinessModel implements OAuthToken {
	private static final long serialVersionUID = -5779707306510231956L;

	@Column(length = 128, nullable = false)
	private String value;

	@Temporal(TemporalType.TIMESTAMP)
	private Date expiration;

	@Column
	private String clientId;

	@Column(length = 100)
	private String scopes;

	@Column(length = 64)
	private String userUuid;

	@Column(length = 200)
	private String redirectUri;

	@Override
	public String getClientId() {
		return this.clientId;
	}

	public Date getExpiration() {
		return this.expiration;
	}

	@Override
	public String getRedirectUri() {
		return this.redirectUri;
	}

	@Override
	public String getScopes() {
		return this.scopes;
	}

	@Override
	public String getUserUuid() {
		return this.userUuid;
	}

	public String getValue() {
		return this.value;
	}

	public void setClientId(final String clientId) {
		this.clientId = clientId;
	}

	public void setExpiration(final Date expiration) {
		this.expiration = expiration;
	}

	public void setRedirectUri(final String redirectUri) {
		this.redirectUri = redirectUri;
	}

	public void setScopes(final String scopes) {
		this.scopes = scopes;
	}

	public void setUserUuid(final String userUuid) {
		this.userUuid = userUuid;
	}

	public void setValue(final String value) {
		this.value = value;
	}

	public OAuth2RefreshToken toToken() {
		if (this.expiration == null) {
			final DefaultOAuth2RefreshToken refreshToken = new DefaultOAuth2RefreshToken(this.value);
			return refreshToken;
		} else {
			final DefaultExpiringOAuth2RefreshToken refreshToken = new DefaultExpiringOAuth2RefreshToken(this.value, this.expiration);
			return refreshToken;
		}
	}
}
