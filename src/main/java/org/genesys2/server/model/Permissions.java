/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.model;

public enum Permissions {
	CREATE("Create"), READ("Read"), UPDATE("Update"), DELETE("Delete");

	String label;

	Permissions(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}

	public static Permissions getByLabel(String value) {
		for (final Permissions permissions : values()) {
			if (permissions.label.equals(value)) {
				return permissions;
			}
		}
		throw new IllegalArgumentException(value);
	}

	public String getName() {
		return name();
	}
}
