/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.model.filters;

public class BasicFilter implements GenesysFilter {

	private final String name;
	private final DataType dataType;
	private FilterType filterType;
	private final Integer maxLength;
	private boolean analyzed = false;

	@Override
	public boolean isCore() {
		return true;
	}

	/**
	 * Is the field analyzed by indexer?
	 */
	@Override
	public boolean isAnalyzed() {
		return this.analyzed;
	};

	public BasicFilter setAnalyzed(boolean analyzed) {
		this.analyzed = analyzed;
		return this;
	}

	public BasicFilter(String name, DataType type) {
		this.name = name;
		this.dataType = type;
		if (this.dataType == DataType.NUMERIC) {
			this.analyzed = false;
			this.filterType = FilterType.RANGE;
		} else {
			this.filterType = FilterType.EXACT;
		}
		this.maxLength = null;
	}

	public BasicFilter(String name, DataType type, FilterType filterType) {
		this.name = name;
		this.dataType = type;
		this.filterType = filterType;
		this.maxLength = null;
	}

	public BasicFilter(String name, DataType type, int i) {
		this.name = name;
		this.dataType = type;
		if (this.dataType == DataType.NUMERIC) {
			this.filterType = FilterType.RANGE;
		} else {
			this.filterType = FilterType.EXACT;
		}
		this.maxLength = i;
	}

	@Override
	public String getKey() {
		return name;
	}

	@Override
	public DataType getDataType() {
		return dataType;
	}

	@Override
	public FilterType getFilterType() {
		return filterType;
	}

	public Integer getMaxLength() {
		return maxLength;
	}
}