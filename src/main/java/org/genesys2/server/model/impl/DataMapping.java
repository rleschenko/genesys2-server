/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.model.impl;

import java.util.Map;

public class DataMapping {
	ColumnGroups grouping;
	Map<Integer, Long> descriptorMap;

	public ColumnGroups getGrouping() {
		return grouping;
	}

	public void setGrouping(final ColumnGroups grouping) {
		this.grouping = grouping;
	}

	public Map<Integer, Long> getDescriptorMap() {
		return descriptorMap;
	}

	public void setDescriptorMap(final Map<Integer, Long> descriptorMap) {
		this.descriptorMap = descriptorMap;
	}
}
