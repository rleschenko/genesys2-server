/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.model.impl;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.genesys2.server.model.AuditedModel;
import org.hibernate.annotations.Type;

@Entity
@Table(name = "article")
public class Article extends AuditedModel {

	private static final long serialVersionUID = 8690395020204070378L;

	@ManyToOne(cascade = {}, optional = false)
	@JoinColumn(name = "classPk")
	private ClassPK classPk;

	@Column
	private Long targetId;

	/**
	 * A descriptive unique URL to the article
	 */
	@Column(nullable = false, length = 150)
	private String slug;

	@Column(nullable = false, length = 6)
	private String lang;

	@Lob
	@Type(type = "org.hibernate.type.TextType")
	private String title;

	@Lob
	@Type(type = "org.hibernate.type.TextType")
	@Column(length = 100000)
	private String body;

	@Temporal(TemporalType.TIMESTAMP)
	private Calendar postDate;

	public String getSlug() {
		return slug;
	}

	public void setSlug(String slug) {
		this.slug = slug;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public Calendar getPostDate() {
		return postDate;
	}

	public void setPostDate(Calendar postDate) {
		this.postDate = postDate;
	}

	public ClassPK getClassPk() {
		return classPk;
	}

	public void setClassPk(ClassPK classPk) {
		this.classPk = classPk;
	}

	public Long getTargetId() {
		return targetId;
	}

	public void setTargetId(Long targetId) {
		this.targetId = targetId;
	}

	public String getLang() {
		return lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}
}
