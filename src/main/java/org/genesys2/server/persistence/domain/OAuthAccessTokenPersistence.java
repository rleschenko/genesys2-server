/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.persistence.domain;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.genesys2.server.model.oauth.OAuthAccessToken;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public interface OAuthAccessTokenPersistence extends JpaRepository<OAuthAccessToken, Long> {

	Collection<OAuthAccessToken> findByClientId(String clientId);

	Collection<OAuthAccessToken> findByUserUuid(String userUuid);

	OAuthAccessToken findByAuthenticationId(String authenticationId);

	@Query("delete from OAuthAccessToken where id in (:ids)")
	@Modifying
	void deleteByIds(@Param("ids") List<Long> ids);

	@Query("delete from OAuthAccessToken where refreshToken = :value")
	@Modifying
	void deleteByRefreshToken(@Param("value") String value);

	@Query("select refreshToken from OAuthAccessToken where value in (:ids)")
	List<String> getRefreshTokensByIds(@Param("ids") List<Long> ids);

	OAuthAccessToken findByValue(String value);

	@Query("delete from OAuthAccessToken where value = :value")
	@Modifying
	void deleteByValue(@Param("value") String value);

	@Query("delete from OAuthAccessToken where clientId = ?1")
	@Modifying
	void deleteByClientId(String clientId);

	@Modifying
	@Query("delete from OAuthAccessToken where expiration < ?1")
	int deleteOlderThan(Date date);

}
