/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.service;

import java.util.Collection;
import java.util.List;

import org.genesys2.server.model.elastic.AccessionDetails;
import org.genesys2.server.service.impl.FilterHandler.AppliedFilters;
import org.genesys2.server.service.impl.SearchException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.facet.result.TermResult;

public interface ElasticService {

	Page<AccessionDetails> search(String query, Pageable pageable) throws SearchException;

	void remove(String className, long id);

	void updateAll(String className, Collection<Long> bucket);

	void refreshIndex(String className);

	Page<AccessionDetails> filter(AppliedFilters appliedFilters, Pageable pageable) throws SearchException;

	TermResult termStatistics(AppliedFilters appliedFilters, String term, int size) throws SearchException;

	TermResult termStatisticsAuto(AppliedFilters appliedFilters, String term, int size) throws SearchException;

	List<String> autocompleteSearch(String query) throws SearchException;

}
