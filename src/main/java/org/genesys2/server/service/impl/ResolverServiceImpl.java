package org.genesys2.server.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.genesys2.server.model.IdUUID;
import org.genesys2.server.model.genesys.Accession;
import org.genesys2.server.model.genesys.AccessionData;
import org.genesys2.server.model.genesys.AccessionHistoric;
import org.genesys2.server.model.impl.FaoInstitute;
import org.genesys2.server.persistence.domain.AccessionHistoricRepository;
import org.genesys2.server.persistence.domain.AccessionRepository;
import org.genesys2.server.service.ResolverService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true)
public class ResolverServiceImpl implements ResolverService {

	@Autowired
	private AccessionRepository repoAccession;

	@Autowired
	private AccessionHistoricRepository repoAccessionHistoric;

	@Override
	public IdUUID forward(UUID uuid) {
		Accession accession = repoAccession.findOneByUuid(uuid);
		if (accession != null) {
			return accession;
		}

		AccessionHistoric accessionHistoric = repoAccessionHistoric.findOneByUuid(uuid);
		if (accessionHistoric != null) {
			return accessionHistoric;
		}

		return null;
	}

	@Override
	public List<AccessionData> findMatches(FaoInstitute faoInstitute, String acceNumb) {
		List<AccessionData> matches = new ArrayList<AccessionData>();
		if (faoInstitute != null) {
			matches.addAll(repoAccession.findByInstituteAndAccessionName(faoInstitute, acceNumb));
			matches.addAll(repoAccessionHistoric.findByInstituteAndAccessionName(faoInstitute, acceNumb));
		} else {
			matches.addAll(repoAccession.findByAccessionName(acceNumb));
			matches.addAll(repoAccessionHistoric.findByAccessionName(acceNumb));
		}
		return matches;
	}

}
