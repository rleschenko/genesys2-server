/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.service;

import java.io.Writer;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.genesys2.server.model.EntityId;
import org.genesys2.server.model.impl.ActivityPost;
import org.genesys2.server.model.impl.Article;
import org.genesys2.server.model.impl.ClassPK;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ContentService {

	List<ActivityPost> lastNews();

	ClassPK ensureClassPK(Class<?> clazz);

	/**
	 * Load article with {@link ClassPK} of clazz with specified id and the
	 * "slug" for the locale
	 *
	 * @param target
	 * @param slug
	 * @param locale
	 * @return
	 */
	Article getArticle(Class<?> clazz, Long id, String slug, Locale locale, boolean useDefault);

	Article getArticle(EntityId entity, String slug, Locale locale);

    Article getArticleBySlugAndLang(String slug, String lang);

	/**
	 * Global articles have ClassPK of Article.class and targetId of null
	 *
	 * @param slug
	 * @param locale
	 * @param useDefault
	 *            Load article from default language
	 * @return
	 */
	Article getGlobalArticle(String slug, Locale locale, boolean useDefault);

	/**
	 * Loads a global article in the specified locale, or when not found the
	 * default locale
	 *
	 * @param string
	 * @param locale
	 * @return
	 */
	Article getGlobalArticle(String string, Locale locale);

	Page<Article> listArticles(Pageable pageable);

    Page<Article> listArticlesByLang(String lang, Pageable pageable);

	void save(Iterable<Article> articles);

	/**
	 * Create new activity post
	 *
	 * @param title
	 * @param body
	 * @return
	 */
	ActivityPost createActivityPost(String title, String body);

	Article updateArticle(EntityId entity, String slug, String title, String body, Locale locale);

	Article updateArticle(Class<?> clazz, Long id, String slug, String title, String body, Locale locale);

	Article updateArticle(long id, String slug, String title, String body);

	Article createGlobalArticle(String slug, Locale locale, String title, String body);

	Article updateGlobalArticle(String slug, Locale locale, String title, String body);
	
	ActivityPost getActivityPost(long id);

	ActivityPost updateActivityPost(long id, String title, String body);

	void deleteActivityPost(long id);

	/**
	 * Returns default locale
	 *
	 * @return
	 */
	Locale getDefaultLocale();

	String processTemplate(String body, Map<String, Object> root);

	void processTemplate(String templateStr, Map<String, Object> root, Writer writer);
}
