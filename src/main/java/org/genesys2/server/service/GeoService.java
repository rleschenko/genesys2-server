/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.service;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import org.genesys2.server.model.impl.Country;
import org.genesys2.server.model.impl.FaoInstitute;
import org.genesys2.server.model.impl.ITPGRFAStatus;

import com.fasterxml.jackson.databind.node.ArrayNode;

public interface GeoService {

	// List<Region> findAllSubregions(Region region);
	//
	// List<Country> findAllCountriesInRegion(Region region);
	//
	// List<Region> findContinents();

	/**
	 * Update ISO countries
	 *
	 * @throws IOException
	 */
	void updateCountryData() throws IOException;

	/**
	 * Get country by ISO CODE (either 2 or 3)
	 *
	 * @param isoCode
	 * @return
	 */
	Country getCountry(String isoCode);

	/**
	 * Tries to find country by ISO CODE2, ISO CODE3, Country name and
	 * translations
	 *
	 * @param findCountry
	 * @return
	 */
	Country findCountry(String countryString);

	List<Country> listAll();

	void updateBlurp(Country country, String blurp, Locale locale);

	List<Country> listAll(Locale locale);

	/**
	 * Lists only "current" entries
	 *
	 * @param locale
	 * @return
	 */
	List<Country> listActive(Locale locale);

	Country getCountryByRefnameId(long refnameId);

	List<Long> listCountryRefnameIds();

	void updateCountryNames(String isoCode3, String jsonTranslations);

	Country updateCountryWiki(String code3, String wiki);

	/**
	 * Get active country, following "replacedBy" where possible.
	 *
	 * @param ISO3
	 *            code
	 * @return
	 */
	Country getCurrentCountry(String code3);

	ArrayNode toJson(List<FaoInstitute> members);

	/**
	 * Lists members to the ITPGRFA
	 *
	 * @param locale
	 * @return
	 */
	List<Country> listITPGRFA(Locale locale);

	/**
	 * Upsert country ITPGRGA status
	 *
	 * @param country
	 * @param contractingParty
	 * @param membership
	 * @param membershipBy
	 * @param nameOfNFP
	 * @return
	 */
	ITPGRFAStatus updateITPGRFA(Country country, String contractingParty, String membership, String membershipBy, String nameOfNFP);

	ITPGRFAStatus getITPGRFAStatus(Country country);

	long countActive();

	String filteredKml(String jsonFilter);

	List<Country> autocomplete(String ac);

}
