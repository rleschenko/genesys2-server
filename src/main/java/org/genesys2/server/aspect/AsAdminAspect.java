/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.aspect;

import java.util.Arrays;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.genesys2.server.model.UserRole;
import org.genesys2.server.model.impl.User;
import org.genesys2.server.security.AuthUserDetails;
import org.genesys2.server.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class AsAdminAspect {
	private static final Logger LOG = LoggerFactory.getLogger(AsAdminAspect.class);

	@Autowired
	private UserService userService;

	// Our copy of the SYS_ADMIN account
	private static Authentication SYS_ADMIN = null;

	@Around("org.genesys2.server.aspect.SystemArchitecture.allServices()" + " && @annotation(org.genesys2.server.aspect.AsAdmin)")
	public Object authenticateAsAdmin(ProceedingJoinPoint pjp) throws Throwable {

		getSystemAdminAccount();

		// store previous version of auth (if any exists)
		final Authentication prevAuth = SecurityContextHolder.getContext().getAuthentication();
		boolean swapped = false;

		// check whether it's not SYS_ADMIN already
		// FIXME check if prevAuth has ADMIN role
		if (prevAuth == null || !prevAuth.getName().equals(SYS_ADMIN.getName())) {
			LOG.warn("Granting ADMIN privileges");
			swapped = true;

			// set new role with admin capabilities
			SecurityContextHolder.getContext().setAuthentication(SYS_ADMIN);
		}

		try {
			// invoke actual code
			return pjp.proceed();
		} finally {
			if (swapped) {
				LOG.warn("Restoring privileges");
				SecurityContextHolder.getContext().setAuthentication(prevAuth);
			}
		}
	}

	public synchronized Authentication getSystemAdminAccount() {
		if (SYS_ADMIN == null) {
			LOG.warn("SYS_ADMIN not loaded. Loading now.");
			final User sysUser = userService.getSystemUser("SYSTEM");

			if (sysUser == null) {
				LOG.warn("Temporary SYS_ADMIN account is being used.");
				SYS_ADMIN = new PreAuthenticatedAuthenticationToken("SYS_ADMIN", null, Arrays.asList(new SimpleGrantedAuthority(UserRole.ADMINISTRATOR
						.getName())));
			} else {
				LOG.warn("Got SYS_ADMIN account: " + sysUser);

				final AuthUserDetails userDetails = new AuthUserDetails(sysUser.getUuid(), "", Arrays.asList(new SimpleGrantedAuthority(UserRole.ADMINISTRATOR
						.getName())));
				userDetails.setUser(sysUser);

				SYS_ADMIN = new PreAuthenticatedAuthenticationToken(userDetails, null, userDetails.getAuthorities());
			}
		}

		return SYS_ADMIN;
	}

}
