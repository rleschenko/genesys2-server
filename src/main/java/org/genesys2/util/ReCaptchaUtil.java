/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.util;

import java.net.InetAddress;
import java.net.UnknownHostException;

import net.tanesha.recaptcha.ReCaptcha;
import net.tanesha.recaptcha.ReCaptchaImpl;
import net.tanesha.recaptcha.ReCaptchaResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * {@link ReCaptcha} wrapper
 *
 * @author matijaobreza
 *
 */
public class ReCaptchaUtil {
	private final static Logger _logger = LoggerFactory.getLogger(ReCaptchaUtil.class);
	private static final String HTTPS_GOOGLE_SERVER = "https://www.google.com/recaptcha/api";

	// Validate the reCAPTCHA
	public static boolean isValid(String captchaPrivateKey, String remoteAddr, String challenge, String response) {
		boolean isLocalRequest = false;

		try {
			final InetAddress remoteInetAddr = InetAddress.getByName(remoteAddr);
			isLocalRequest = remoteInetAddr.isLinkLocalAddress() || remoteInetAddr.isAnyLocalAddress() || remoteInetAddr.isLoopbackAddress();
			_logger.warn("Remote addr: " + remoteAddr + " " + remoteInetAddr + " isLocal=" + isLocalRequest);
		} catch (final UnknownHostException e1) {
			_logger.warn(e1.getMessage());
		}

		if (isLocalRequest) {
			_logger.info("Ignoring localhost re-captcha.");
			return true;
		}

		final ReCaptchaImpl reCaptcha = new ReCaptchaImpl();
		reCaptcha.setRecaptchaServer(HTTPS_GOOGLE_SERVER);
		reCaptcha.setPrivateKey(captchaPrivateKey);

		final ReCaptchaResponse reCaptchaResponse = reCaptcha.checkAnswer(remoteAddr, challenge, response);

		return reCaptchaResponse.isValid();
	}
}
