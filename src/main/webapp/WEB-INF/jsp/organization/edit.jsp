<!DOCTYPE html>

<%@include file="/WEB-INF/jsp/init.jsp"%>

<html>
<head>
<title><spring:message code="organization.page.profile.title" arguments="${organization.slug}" argumentSeparator="|" /></title>
</head>
<body>
	<h1>
		<c:out value="${organization.title}" />
		<small><c:out value="${organization.slug}" /></small>
	</h1>

	<form role="form" class="" action="<c:url value="/org/${organization.slug}/update" />" method="post">
		<div class="form-group">
			<label for="organization-slug" class="control-label"><spring:message code="organization.slug" /></label>
			<div class="controls">
				<input value="<c:out value="${organization.slug}" />" type="text" id="organization-slug" name="slug" class="span9 required form-control" />
			</div>
		</div>
		
		<div class="form-group">
			<label for="organization-title" class="control-label"><spring:message code="organization.title" /></label>
			<div class="controls">
				<input value="<c:out value="${organization.title}" />" id="organization-title" name="title" class="span9 required form-control" />
			</div>
		</div>
		
		<div class="form-group">
			<label for="blurp-body" class="control-label"><spring:message code="blurp.blurp-body" /></label>
			<div class="controls">
				<textarea id="blurp-body" name="blurp" class="span9 required html-editor">
					<c:out value="${blurp.body}" />
				</textarea>
			</div>
		</div>

		<input type="submit" value="<spring:message code="blurp.update-blurp"/>" class="btn btn-primary" />
		<a href="<c:url value="/org/${organization.slug}" />" class="btn btn-default"> <spring:message code="cancel" />
		</a>
        <!-- CSRF protection -->
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
	</form>

<content tag="javascript">	
	<script type="text/javascript">
		<local:tinyMCE selector=".html-editor" />
	</script>
</content>

</body>
</html>