<%@ page contentType="text/xml;charset=UTF-8" pageEncoding="UTF-8" language="java" session="false"%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="security" uri="http://www.springframework.org/security/tags"%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%><%@ taglib
	prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%><?xml version="1.0" encoding="UTF-8"?>

<rss version="2.0" 
      xmlns:opensearch="http://a9.com/-/spec/opensearch/1.1/"
      xmlns:atom="http://www.w3.org/2005/Atom">

   <channel>
     <title><spring:message code="search.page.title" />: <c:out value="${q}" /></title>
     <link><c:url value="/acn/search"><c:param name="q" value="${q}" /><c:param name="page" value="${pagedData.number + 1}" /></c:url></link>
     <description>Search results for "New York history" at Example.com</description>
     <opensearch:totalResults><c:out value="${pagedData.totalElements}" /></opensearch:totalResults>
     <opensearch:startPage><c:out value="${pagedData.number + 1}" /></opensearch:startPage>
     <opensearch:itemsPerPage><c:out value="${pagedData.size}" /></opensearch:itemsPerPage>
     <atom:link rel="search" type="application/opensearchdescription+xml" href="<c:url value="/acn/opensearch/desc" />" />
     <opensearch:Query role="request" searchTerms="<c:out value="${a}" />" startPage="<c:out value="${pagedData.number + 1} "/>" />
     
	 <c:if test="${error ne null}">
		<item>
			<title>Error</title>
			<description><spring:message code="search.search-query-failed" arguments="${error.message}" /></description>
		</item>
	</c:if>
	<c:if test="${pagedData eq null}">
		<item>
			<title>Error</title>
			<description><spring:message code="search.search-query-missing" /></description>
		</item>
	</c:if>
	
	<c:forEach items="${pagedData.content}" var="accession" varStatus="status">
     <item>
       <title><c:out value="${accession.acceNumb}" /></title>
       <link><c:url value="/acn/id/${accession.id}" /></link>
       <description>
       <![CDATA[
       	<b><c:out value="${accession.acceNumb}" /></b>
        (<em><c:out value="${accession.taxonomy.sciName}" /></em>),
        <spring:message code="accession.sampleStatus.${accession.sampStat}" />
        <c:out value="${accession.institute.code}" />
        ]]>
       </description>
     </item>
     </c:forEach>
   </channel>
 </rss>