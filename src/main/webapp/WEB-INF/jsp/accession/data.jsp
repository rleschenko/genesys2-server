<!DOCTYPE html>

<%@include file="/WEB-INF/jsp/init.jsp"%>

<html>
<head>
<title><spring:message code="accession.page.data.title" /></title>
</head>
<body>
	<h1>
		<spring:message code="accession.page.data.title" />
	</h1>

	<div class="main-col-header clearfix">
	<div class="nav-header pull-left">
		<div class="results"><spring:message code="accessions.number" arguments="${pagedData.totalElements}" />
		<a href="<c:url value="/explore/map"><c:param name="filter">${jsonFilter}</c:param></c:url>">Map</a></div> 
		<div class="pagination">
			<spring:message code="paged.pageOfPages" arguments="${pagedData.number+1},${pagedData.totalPages}" />
			<a href="<spring:url value=""><spring:param name="page" value="${pagedData.number eq 0 ? 1 : pagedData.number}" /><spring:param name="filter" value="${jsonFilter}" /></spring:url>"><spring:message code="pagination.previous-page" /></a>
			<a href="<spring:url value=""><spring:param name="page" value="${pagedData.number+2}" /><spring:param name="filter" value="${jsonFilter}" /><spring:param name="pick" value="${jsonPick}" /></spring:url>"><spring:message code="pagination.next-page" /></a>
		</div>
	</div>
	</div>
	
	<c:if test="${filters eq null and jsonFilter ne null}">
		<div class="applied-filters">
			<spring:message code="filters.data-is-filtered" />
			<a href="<spring:url value="/explore/filter"><spring:param name="filter" value="${jsonFilter}" /><spring:param name="pick" value="${jsonPick}" /></spring:url>"><spring:message code="filters.modify-filters" /></a>
		</div>
	</c:if>

	<c:if test="${filters ne null}">
		<div class="applied-filters">	
		<c:forEach items="${filters.keySet()}" var="by">
			<c:set value="${filters[by].getClass().simpleName}" var="clazz" />
			<div>
				<spring:message code="${by}" />: <b>
					<c:choose>
						<c:when test="${clazz eq 'Taxonomy'}">
							${filters[by].taxonName}
						</c:when>
						<c:when test="${clazz eq 'Crop'}">
							<a href="<c:url value="/c/${filters[by].shortName}" />">${filters[by].getName(pageContext.response.locale)}</a>
						</c:when>
						<c:when test="${clazz eq 'Country'}">
							<a href="<c:url value="/geo/${filters[by].code3}" />"><c:out value="${filters[by].getName(pageContext.response.locale)}" /></a>
						</c:when>
						<c:when test="${clazz eq 'FaoInstitute'}">
							<a href="<c:url value="/wiews/${filters[by].code}" />"><c:out value="${filters[by].fullName}" /></a>
						</c:when>
						<c:when test="${clazz eq 'Organization'}">
							<a href="<c:url value="/org/${filters[by].slug}" />"><c:out value="${filters[by].title}" /></a>
						</c:when>
						<c:otherwise>
							${filters[by]}
						</c:otherwise>
					</c:choose>
				</b>
			</div>
			<c:remove var="clazz" />
		</c:forEach>
			<a href="<spring:url value="/explore/filter"><spring:param name="filter" value="${jsonFilter}" /><spring:param name="pick" value="${jsonPick}" /></spring:url>"><spring:message code="filters.modify-filters" /></a>
		</div>
	</c:if>

	<table class="accessions">
		<thead>
			<tr>
				<td class="idx-col"></td>
				<td />
				<td><spring:message code="accession.accessionName" /></td>
				<td><spring:message code="accession.taxonomy" /></td>
				<td class="notimportant"><spring:message code="accession.origin" /></td>
				<td class="notimportant"><spring:message code="accession.sampleStatus" /></td>
				<td class="notimportant"><spring:message code="accession.holdingInstitute" /></td>
				<%-- 				<td><spring:message code="accession.holdingCountry" /></td>
 --%>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${pagedData.content}" var="accession" varStatus="status">
				<tr class="acn ${status.count % 2 == 0 ? 'even' : 'odd'}">
					<td class="idx-col">${status.count + pagedData.size * pagedData.number}</td>
					<td class="sel" x-aid="${accession.id}"></td>
					<td><a href="<c:url value="/acn/id/${accession.id}" />"><b><c:out value="${accession.accessionName}" /></b></a></td>
					<%-- <td><a href="<c:url value="/acn/t/${accession.taxonomy.genus}/${accession.taxonomy.species}" />"><c:out value="${accession.taxonomy.taxonName}" /></a></td> --%>
					<td><c:out value="${accession.taxonomy.taxonName}" /></td>
					<%-- <td class="notimportant"><a href="<c:url value="/geo/${accession.origin}" />"><c:out value="${accession.countryOfOrigin.name}" /></a></td> --%>
					<td class="notimportant"><c:out value="${accession.countryOfOrigin.getName(pageContext.response.locale)}" /></td>
					<td class="notimportant"><spring:message code="accession.sampleStatus.${accession.sampleStatus}" /></td>
					<td class="notimportant"><a href="<c:url value="/wiews/${accession.institute.code}" />"><c:out value="${accession.institute.code}" /></a></td>
					<%-- 			<td><a href="<c:url value="/geo/${accession.institute.country.code3}" />"><c:out value="${accession.institute.country.name}" /></a></td>
		 --%>
				</tr>
			</c:forEach>
		</tbody>
	</table>

</body>
</html>