<!DOCTYPE html>

<%@include file="/WEB-INF/jsp/init.jsp"%>

<html>
<head>
<title><spring:message code="accession.page.resolve.title" /></title>
</head>
<body>
	<h1><spring:message code="accession.resolve" arguments="${accessionName}" /></h1>

	<table>
		<thead>
			<tr>
				<td><spring:message code="accession.accessionName" /></td>
				<td><spring:message code="accession.origin" /></td>
				<td><spring:message code="accession.taxonomy" /></td>
				<td><spring:message code="accession.holdingInstitute" /></td>
				<td><spring:message code="accession.holdingCountry" /></td>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${accessions}" var="accession" varStatus="status">
				<tr>
					<td><a href="<c:url value="/acn/id/${accession.id}" />"><b><c:out value="${accession.accessionName}" /></b></a></td>
					<td><c:out value="${accession.countryOfOrigin.getName(pageContext.response.locale)}" /></td>
					<td><c:out value="${accession.taxonomy.taxonName}" /></td>
					<td><a href="<c:url value="/wiews/${accession.institute.code}" />"><c:out value="${accession.institute.code}" /></a></td>
					<td><a href="<c:url value="/geo/${accession.institute.country.code3}" />"><c:out value="${accession.institute.country.getName(pageContext.response.locale)}" /></a></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>

</body>
</html>