
update accession set ACC_Numb_HI=concat('ICG ', ACC_Numb_HI) where Institute='IND002' and Genuss='Arachis';
update accession set ACC_Numb_HI=concat('ICP ', ACC_Numb_HI) where Institute='IND002' and Genuss='Cajanus';
update accession set ACC_Numb_HI=concat('ICC ', ACC_Numb_HI) where Institute='IND002' and Genuss='Cicer';
update accession set ACC_Numb_HI=concat('IEc ', ACC_Numb_HI) where Institute='IND002' and Genuss='Echinochloa';
update accession set ACC_Numb_HI=concat('IE ', ACC_Numb_HI) where Institute='IND002' and Genuss='Eleusine';
update accession set ACC_Numb_HI=concat('IPs ', ACC_Numb_HI) where Institute='IND002' and Genuss='Paspalum';
update accession set ACC_Numb_HI=concat('IP ', ACC_Numb_HI) where Institute='IND002' and Genuss='Pennisetum';
update accession set ACC_Numb_HI=concat('ISe ', ACC_Numb_HI) where Institute='IND002' and Genuss='Setaria';
update accession set ACC_Numb_HI=concat('IS ', ACC_Numb_HI) where Institute='IND002' and Genuss='Sorghum';
update accession set ACC_Numb_HI=concat('IPmr ', ACC_Numb_HI) where Institute='IND002' and Taxon_Code in (select Taxon_Code from taxonomy where genus='Panicum' and species='sumatrense');
update accession set ACC_Numb_HI=concat('IPm ', ACC_Numb_HI) where Institute='IND002' and Taxon_Code in (select Taxon_Code from taxonomy where genus='Panicum' and species='miliaceum');
